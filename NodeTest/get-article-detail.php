<?php
ini_set('display_errors',0);
ini_set('display_startup_errors',0);
error_reporting(E_ALL);

include "../Helpers/CategoryHelper.php";
include "../Helpers/SchoolHelper.php";
include "../Helpers/LegacyMathHelper.php";
include "../Helpers/LanguageHelper.php";

class Constants {
	public static $dataDir = "/export/www/igi/";
}

interface iArticleDetailFacade {
	public function getDetail($articleId,$space);
}

//For current use: filesystem implementation
class ArticleDetailFacadeFileSystem implements iArticleDetailFacade {

	private function isFileExists($fullPath) {		
		return file_exists($fullPath);
	}

	private function getInternDetail($articleId,$space) {
			$fullPath = Constants::$dataDir.$space."/".$articleId.".json";
		if($this->isFileExists($fullPath))
		return file_get_contents($fullPath);	
		
		else return null;
	}

	private function getModifiedDetailForProduction($articleId,$space) {
		$internDetailJson = json_decode($this->getInternDetail($articleId,$space),true);	
		if($internDetailJson == null)
			return null;
		$arrForJsonProduction['id'] = $internDetailJson['articleId'];	
		$arrForJsonProduction['created_at'] = $internDetailJson['displaytime'];	
		$arrForJsonProduction['title'] = $internDetailJson['title'];	
		$arrForJsonProduction['authors'] = $internDetailJson['authors'];
		/*if(isset($internDetailJson['links']))
			$arrForJsonProduction['links'] = $internDetailJson['links'];*/
		if(isset($internDetailJson['images']))
			$arrForJsonProduction['images'] = $internDetailJson['images'];
		if(isset($internDetailJson['sources']))
			$arrForJsonProduction['sources'] = $internDetailJson['sources'];
		$arrForJsonProduction['content'] = $internDetailJson['content'];
		$arrForJsonProduction['a4_count'] = $internDetailJson['a4_count'];
		
		$contentFull = "";	
		foreach($arrForJsonProduction['content'] as $value) {
			$contentFull .= $value['value'];
		}
		
		$languageHelper =  LanguageHelper::get();
		
		$arrForJsonProduction['language'] = $languageHelper->getDetail($internDetailJson['language']['id']);

		$arrForJsonProduction['word_count'] = LegacyMathHelper::countwords($contentFull);
		$arrForJsonProduction['fast_reading_time'] = LegacyMathHelper::count_fast_reading_time($arrForJsonProduction['a4_count']);
		$arrForJsonProduction['slow_reading_time'] = LegacyMathHelper::count_slow_reading_time($arrForJsonProduction['a4_count']);
		$arrForJsonProduction['fast_reading_time_sec'] = LegacyMathHelper::count_fast_reading_time_sec($arrForJsonProduction['a4_count']);
		$arrForJsonProduction['slow_reading_time_sec'] = LegacyMathHelper::count_slow_reading_time_sec($arrForJsonProduction['a4_count']);

		$categoryHelper = CategoryHelper::get();
		$categoryTree = $categoryHelper->getCategoryTree($internDetailJson['category']/*"vs_podohospodarstvo-a-veterina_polnohospodarstvo"*/);
		$arrForJsonProduction['category'] = $categoryTree;

		
		if(isset($internDetailJson['school']) && $internDetailJson['school'] != null) {
			$schoolHelper = SchoolHelper::get();		
			$schoolData = $schoolHelper->getSchoolData($internDetailJson['school']/*"zakladna-skola-s-ms-358"*/);
			$arrForJsonProduction['school'][$internDetailJson['school']/*"zakladna-skola-s-ms-358"*/] =  $schoolData;
		}	

		return 	json_encode($arrForJsonProduction);

		
		
		
	}


	public function getDetail($articleId,$space) {
		return $this->getModifiedDetailForProduction($articleId,$space); 
	} 
}

class ArticleDetailContext {



	public function getDetail($articleId,$space,$articleDetailFacade) {
		$bean = $articleDetailFacade->getDetail($articleId,$space);
		return $bean;	
	}

}

//--------- MAIN SCRIPT ----------------------------
$context =  new ArticleDetailContext();
// for different source handler implement new ArticleDetailFacade*
$articleDetailFacade = new  ArticleDetailFacadeFileSystem();
// inject ArticleDetailFacade
$articleDetail = $context->getDetail($_REQUEST['id'],"article_referaty",$articleDetailFacade);

header("Cache-Control: no-cache, must-revalidate");
if($articleDetail == null) {
	header("HTTP/1.0 404 Not Found");
} else {
	
	header('Content-Type: application/json;charset=utf-8');
	echo $articleDetail;
}


?>
