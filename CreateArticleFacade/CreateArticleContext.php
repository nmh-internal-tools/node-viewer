<?php

include "../NodeManagerClient/NodeManagerFacade.php";

class CreateArticleContext  {

	

	public function setNodeManagerFacade($nodeManagerFacade) {
		$this->nodeManagerFacade = $nodeManagerFacade;	
	}


	public function CreateArticleContext() {

	}

	public function getCreateArticleControllerBean($documentUrl) {
		$bean =  new CreateArticleController();
		$bean->setNode($this->getNode($documentUrl));
		return  $bean; 
	}

	private function getNode($documentUrl) {
		$facade = new NodeManagerFacade();
		$bean = $facade->getNode($documentUrl);
		return $bean;	
	}

}

?>
