<?
include "../Constants/Constants.php";
include "../Helpers/LegacyMathHelper.php";
ini_set('display_errors', 1); 

class CreateArticleController {

	private $node = null;

	public function setNode($node) {
		$this->node = $node;
	}


	private function getArticle() {
	
		$displayTime = $this->node->getProperty("displaytime");
			if(!is_null($displayTime)) {
				$displayTime = $this->node->getProperty("displaytime")->getValue()->getLong();
			}	
			
		$createdTime = $this->node->getProperty("created_time");
			if(!is_null($createdTime)) {
				$createdTime = $this->node->getProperty("created_time")->getValue()->getLong();
			}	
			
		$legacyId = $this->node->getProperty("legacy_id");
			if(!is_null($legacyId)) {
				$legacyId = $this->node->getProperty("legacy_id")->getValue()->getLong();
			}
			
		$userId = $this->node->getProperty("user_id");
			if(!is_null($userId)) {
				$userId = $this->node->getProperty("user_id")->getValue()->getLong();
			}	
			
			
		 $userIdIsLegacy = $this->node->getProperty("user_id_islegacy");
			if(!is_null($userIdIsLegacy)) {
				$userIdIsLegacy = $this->node->getProperty("user_id_islegacy")->getValue()->getBoolean();
				
			} 	

		$arrForJson['displaytime'] = $displayTime;
		//$arrForJson['createdTime'] = $createdTime;
		$arrForJson['legacyId'] = $legacyId;
		$arrForJson['userId'] = $userId;
		$arrForJson['userIdIsLegacy'] = $userIdIsLegacy;

		$arrForJson['state'] = $this->node->getPropertyValueAsString("state");
		$arrForJson['space'] = $this->node->getPropertyValueAsString("space");
		$arrForJson['category'] = $this->node->getPropertyValueAsString("category");
		//$arrForJson['parentCategory'] = $this->node->getPropertyValueAsString("original_category");
		//$arrForJson['keywords'] = $this->node->getPropertyValueAsString("keywords");
		$arrForJson['legacyId'] = $this->node->getProperty("legacy_id")->getValue()->getLong();
		$arrForJson['language']['id'] = $this->node->getPropertyValueAsString("language");
		$arrForJson['documentUrl'] = $this->node->getUrl();
		$arrForJson['articleId'] = $this->node->getPropertyValueAsString("article_id");
		//$arrForJson['note'] = $this->node->getPropertyValueAsString("note");
		$arrForJson['type'] = $this->node->getPrimaryType()->getName();
		$arrForJson['title'] = $this->node->getPropertyValueAsString("title");

		$arrForJson['school'] = $this->node->getPropertyValueAsString("school");
		

		/**if($this->node->getChild("schools") != null) {
			
		foreach($this->node->getChild("schools")->getProperties() as $key=>$value)  {
			$arrForJson['schools'][] = $value->value[0]->string;			
			}
		}**/

		if($this->node->getChild("authors") != null) {
			//var_dump($this->node->getChild("authors"));
			foreach($this->node->getChild("authors")->getProperties() as $key=>$value)  {
				$arrForJson['authors']['id'][] = $value->value[0]->string;	
			}
		
		} else $arrForJson['authors'] = array();

		if($this->node->getChild("links") != null) {
			
		foreach($this->node->getChild("links")->getChildren() as $key=>$value)  {
		
			$text = $value->getChild("text")->getProperty("value")->getValue()->getString();
			$url = $value->getChild("url")->getProperty("value")->getValue()->getString();
			$arrForJson['links'][] = array("text"=>$text,"url"=>$url);

		}  
		
	}
	
	if($this->node->getChild("images") != null) {
		//var_dump($this->node->getChild("images"));
		foreach($this->node->getChild("images")->getChildren() as $key=>$value)  {
			$idImage = null;
			$title = "";			
			if($value->getChild("id") != null)
				$idImage = $value->getChild("id")->getProperty("value")->getValue()->getString();
			if($value->getChild("title") != null)			
				$title = $value->getChild("title")->getProperty("value")->getValue()->getString();	
			$arrForJson['images'][] = array("id"=>$idImage,"description"=>$title);
		}
	
	} 


	if($this->node->getChild("sources") != null) {
		foreach($this->node->getChild("sources")->getChildren() as $key=>$value)  {
			$title = "";
			$url = "";
			if($value->getChild("title") != null)
				$title = $value->getChild("title")->getProperty("value")->getValue()->getString();
			if($value->getChild("url") != null)
				$url = $value->getChild("url")->getProperty("value")->getValue()->getString();
			
			$arrForJson['sources'][] = array("description"=>$title,"url"=>$url);
			
			}
		
		} 
		
		//FROM OLD VERSION - paging
		$t_content = preg_replace("/<\/?i(.|\s)*?>/","",$this->node->getPropertyValueAsString("content2"));
		$t_content = str_replace("<HR>","<hr>",$t_content);
		$t_content = str_replace("<hr />","<hr>",$t_content);
		$t_content = trim(ltrim($t_content));
		$content = explode("<hr>", str_replace("\n","<br>",stripslashes($t_content)));/*ConvertHtml*/
		$arrForJson['a4_count'] = LegacyMathHelper::count_pages_in_html($t_content);
		$arrForJson['word_count'] = LegacyMathHelper::countwords($t_content);

		$arrForJson['content']['countPages'] = count($content);
		foreach($content as $l_key => $l_value)
		{
			
			$l_value=preg_replace("'style=\".*?\"'i", "", $l_value);
			$l_value=preg_replace("'face=\".*?\"'i", "", $l_value);
			$key = ($l_key+1);
			$arrForJson['content'][$key] = array("index"=>$key,"value"=>$l_value);	
		}
		//END - paging
		
		
		
		
		$arr['articleId'] = $this->node->getPropertyValueAsString("article_id");
		$arr['data'] = $arrForJson;
		
		
		
		return $arr; 

	}	


	/**
	** It saves article and annotation on filesystem
	**/
	public function saveArticle() {

		
		$arrArticle = $this->getArticle();
		 try {
			$fp = fopen(Constants::$dataDir.$arrArticle['articleId'].".json","wb");
			if(!$fp) {
        			throw new Exception("Could not open the file!: ".Constants::$dataDir.$arrArticle['articleId'].".json");
			    }	
		
			fwrite($fp,json_encode($arrArticle['data']));
			fclose($fp);

		} catch (Exception $e) {
       			 echo "Error (File: ".$e->getFile().", line ".
         		 $e->getLine()."): ".$e->getMessage();
		} 

	}

} 

?>
