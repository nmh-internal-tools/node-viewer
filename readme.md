### NodeViewer GUI for Aktualne.sk and Referaty.sk      
*Copyright 2016 News and Media Holding, a.s. (Terezia Ichnatoliova)*
#### 1. General information  
NodeViewer is GUI tool for programmers to retrieve any node (article) based on Zikkurat architecture. Currently it works for aktualne.sk and referaty.sk. Mandatory inputs are hessian URL and document URL (node ID). Optional input is revision. There are few ways how to display the node (e.g. raw dump, table, tree, etc).  
#### 2. Configuration  
Configuration file consists of Hessian URLs located in `constants/NodeViewerConstants.php`.     
#### 3. Installation  
No installation required.
#### 4. Technologies required
      - PHP >= 5.3
      - MySQL
      - HTML5
      - CSS3  
#### 5. How to use the Service   
Html is executable from `NodeManagerClient/NodeViewer.php` and it is manageable from GUI.
#### 6. How to test  
Internal tools do not have any testing environment.
#### 7. Where is it deployed    
**host**: `10.10.20.17` (Makac)     
**location**: `/export/www/ichnatoliova/ZikkuratPokusy/NodeManagerClient/NodeViewer.php`   
**URL**: `http://10.10.20.8/ichnatoliova/ZikkuratPokusy/NodeManagerClient/NodeViewer.php`   
**vhost**: none
#### 8. Who uses it  
End users through web app.
#### 9. Logging 
No logging available.