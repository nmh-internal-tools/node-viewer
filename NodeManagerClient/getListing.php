<?php
//set_time_limit(0);
ini_set('memory_limit', '-1');
ini_set('display_errors', 1); 
ini_set("max_execution_time", 0);

include "../Hessian/HessianClient.php";

class getListing  {
	
	private static $SERVICE_URL = "http://10.10.20.56:14091/ZikkuratIndexApp/public/getListing";
	
	private static $singleton;
	private $proxy;
	
	public function getListing(){
		$options = new HessianOptions();
		$this->proxy = new HessianClient(getListing::$SERVICE_URL,$options);
	}
	
	public function getAll($filters){
		return $this->proxy->getAll($filters);
	}
	
	
	
	public static function get(){
		if(getListing::$singleton == null){
			getListing::$singleton = new getListing();
		}
		return getListing::$singleton;
	}
	
}


$listing = new getListing();

$filters = array("authors"=>array(),"category"=>array("pedagogika"),"displayTime"=>null);

$arr = $listing->getAll($filters);

var_dump($arr['listObjects'][0]['title']);
var_dump($arr['metaData']);
?>
