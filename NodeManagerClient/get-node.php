<?php
    ini_set("display_errors", 1);
	require "NodeViewerManagerClient.php";
	require "../Helpers/HttpResponseHelper.php";
	require "../Helpers/DocumentUrlHelper.php";
	
	//-----------MAIN SCRIPT---------------
	
	$hessianUrl = $_GET['hessian'];
	$documentUrl = $_GET['doc_url'];
	$articleId = $_GET['article_id'];
	$publicUri = $_GET['public_uri'];
	$revision = $_GET['revision'];
	$display = $_GET['display'];

	if ($hessianUrl==null || $hessianUrl=="") {
		\Http\HttpResponseHelper::clientResponse("404");
	}
	else {
		$proxy = new NodeViewerManagerClient($hessianUrl);
		if (($documentUrl==null || $documentUrl=="") && ($articleId==null || $articleId=="") && ($publicUri==null || $publicUri=="")) {
			\Http\HttpResponseHelper::clientResponse("404");
		}
		else {
			if ($documentUrl!=null || $documentUrl!="") {
				if ($revision==null || $revision=="") {
					$node=$proxy->getNode($documentUrl);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				elseif ((int)$revision>0) {
					$node = $proxy->getNodeRevision($documentUrl,(int)$revision);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				else {
					\Http\HttpResponseHelper::clientResponse("404");
				}
			}
			elseif ($articleId!=null || $articleId!="") {
				$documentUrlHelper=new DocumentUrlHelper();
				$documentUrl=$documentUrlHelper->getDocumentUrlByArticleId($articleId);
				if ($revision==null || $revision=="") {
					$node=$proxy->getNode($documentUrl);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				elseif ((int)$revision>0) {
					$node = $proxy->getNodeRevision($documentUrl,(int)$revision);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				else {
					\Http\HttpResponseHelper::clientResponse("404");
				}
			}
			elseif ($publicUri!=null || $publicUri!="") {
				$documentUrlHelper=new DocumentUrlHelper();
				$articleId=$documentUrlHelper->getArticleIdByPublicUri($publicUri);
				$documentUrl=$documentUrlHelper->getDocumentUrlByArticleId($articleId);
				if ($revision==null || $revision=="") {
					$node=$proxy->getNode($documentUrl);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				elseif ((int)$revision>0) {
					$node = $proxy->getNodeRevision($documentUrl,(int)$revision);
					\Http\HttpResponseHelper::clientResponse("json",json_encode($node,true));
				}
				else {
					\Http\HttpResponseHelper::clientResponse("404");
				}
			}
			else {
				\Http\HttpResponseHelper::clientResponse("404");
			}
		}
	}
?>