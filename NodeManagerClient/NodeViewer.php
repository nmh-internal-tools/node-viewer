<!DOCTYPE html>
<html>
<head>
<title>Node Viewer</title>
<meta charset="UTF-8">
<style>
	.rightn {padding-left: 30px;}
	#nodeviewer {font-size: 16pt; padding-left: 30px;}
	img {width: 100%; margin-right: 0px;}
	.longinput {width: 700px;}
	.mediuminput {width: 300px;}
	.basicinput {height: 20px; background-color: #FFFFFF; border: 1px solid #000000; font-weight: bold; padding-left: 5px;}
	.leftn {text-align: right; vertical-align: text-top; font-weight: bold;}
	.left {text-align: right; vertical-align: text-top; font-weight: bold; background-color: #F0F0F0;}
	.left a:link {text-align: right; vertical-align: text-top; font-weight: bold; color: #606060;}
	.left a:visited {text-align: right; vertical-align: text-top; font-weight: bold; color: #606060;}
	.left a:hover {text-align: right; vertical-align: text-top; font-weight: bold; color: #606060; text-decoration: underline;}
	.left a:active {text-align: right; vertical-align: text-top; font-weight: bold; color: #606060;}
	form {font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif; padding-left: 30px;}
	form input {font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;}
	body {width: 97%; margin-left: auto; margin-right: auto;}
	ul {list-style-image: none; list-style-type: none;}
	#hiddenspan {display: none;}
	.red {color: red;}
	.button {width: 70px; height: 25px; background-color: #FFFFFF; border: 1px solid #000000; font-weight: bold;}
</style>
<script>
	function disableDocumentUrl() {
		if (document.getElementById("referatysk").checked==true) {
			if (document.getElementById("doc_url").value!="") {
				document.getElementById("article_id").disabled = true;
				document.getElementById("public_uri").disabled = true;
			}
			else {
				document.getElementById("article_id").disabled = false;
				document.getElementById("public_uri").disabled = false;
			}
		}
	}
	function disableArticleId() {
		if (document.getElementById("referatysk").checked==true) {
			if (document.getElementById("article_id").value!="") {
				document.getElementById("doc_url").disabled = true;
				document.getElementById("public_uri").disabled = true;
			}
			else {
				document.getElementById("doc_url").disabled = false;
				document.getElementById("public_uri").disabled = false;
			}
		}
	}
	function disablePublicUri() {
		if (document.getElementById("referatysk").checked==true) {
			if (document.getElementById("public_uri").value!="") {
				document.getElementById("doc_url").disabled = true;
				document.getElementById("article_id").disabled = true;
			}
			else {
				document.getElementById("doc_url").disabled = false;
				document.getElementById("article_id").disabled = false;
			}
		}
	}
	function aktualne() {
		if (document.getElementById("aktualnesk").checked == true) {
			document.getElementById("article_id").disabled = true;
			document.getElementById("public_uri").disabled = true;
		}
	}
	function referaty() {
		if (document.getElementById("referatysk").checked == true) {
			document.getElementById("article_id").disabled = false;
			document.getElementById("public_uri").disabled = false;
		}
	}
	function showList() {
		document.getElementById("hiddenspan").style.display = "inline";
	}
	function hideList() {
		document.getElementById("hiddenspan").style.display = "hidden";
	}
	function toggleList() {
		if (document.getElementById("hiddenspan").style.display == "hidden") {
			showList();
		}
		else {
			hideList();
		}
	}
	function color(x) {
	    x.style.backgroundColor = "#F0F0F0";
	}
	function uncolor(x) {
	    x.style.backgroundColor = "#FFFFFF";
	}
</script>
</head>
<body>
	<form method="get">
		<table>
			<tr>
				<td class="leftn"><img src="../img/headerLogoAtlas.jpg" width="119" height="35"/></td><td><b><span id="nodeviewer">NODE VIEWER - </span>Internal tool for fetching documents over Hessian connection</b></td>
			</tr>
		</table>
		<table>
			<tr>
				<td class="leftn">Hessian URL:</td>
				<td class="rightn">
					<input type="radio" name="hessian" value="http://10.10.20.53:14090/ZikkuratCoreReferatyApp/public/NodeManagerService" <?php if(!isset($_GET['hessian']) || (isset($_GET['hessian']) && $_GET['hessian']=='http://10.10.20.53:14090/ZikkuratCoreReferatyApp/public/NodeManagerService'))  echo 'checked="checked"';?> id="referatysk" onchange="referaty()"/><label>referaty.sk</label>
					<input type="radio" name="hessian" value="http://fs.atlas-as.sk:8000/ZikkuratAktualneApp/NodeManagerService" <?php if(isset($_GET['hessian']) && $_GET['hessian']=='http://fs.atlas-as.sk:8000/ZikkuratAktualneApp/NodeManagerService')  echo 'checked="checked"';?> id="aktualnesk" onchange="aktualne()"/><label>aktualne.sk</label>
					<input type="radio" name="hessian" value="http://10.10.20.51:14080/ZikkuratCore/NodeManagerService" <?php if(isset($_GET['hessian']) && $_GET['hessian']=='http://10.10.20.51:14080/ZikkuratCore/NodeManagerService')  echo 'checked="checked"';?> id="aktualnesk" onchange="aktualne()"/><label>aktualne.devel</label>
<input type="radio" name="hessian" value="http://app-java-01.in.new.atlas-as.sk:14080/ZikkuratCore/NodeManagerService" <?php if(isset($_GET['hessian']) && $_GET['hessian']=='http://app-java-01.in.new.atlas-as.sk:14080/ZikkuratCore/NodeManagerService')  echo 'checked="checked"';?> id="aktualnesk" onchange="aktualne()"/><label>aktualne2015</label>
				</td>
			</tr>
			<tr>
				<td class="leftn">Document URL:</td><td class="rightn"><input type="text" class="mediuminput basicinput" name="doc_url" id="doc_url" onchange="disableDocumentUrl()" value="<?php echo htmlspecialchars($_GET['doc_url']); ?>"/> or <span class="leftn">
								Article ID: </span><input type="text" name="article_id" id="article_id" onchange="disableArticleId()" class="basicinput" value="<?php echo htmlspecialchars($_GET['article_id']); ?>"/> or <span class="leftn">
								Public URI: </span><input type="text" name="public_uri" id="public_uri" onchange="disablePublicUri()" class="mediuminput basicinput" value="<?php echo htmlspecialchars($_GET['public_uri']); ?>"/></td>
			</tr>
			<tr>
				<td class="leftn">Revision:</td><td class="rightn"><input type="text" class="basicinput" name="revision" value="<?php echo htmlspecialchars($_GET['revision']); ?>"/></td>
			</tr>
			<tr>
				<td class="leftn">Display:</td>
				<td class="rightn">
					<input type="radio" name="display" value="dump" <?php if(!isset($_GET['display']) || (isset($_GET['display']) && $_GET['display']=='dump'))  echo 'checked="checked"';?>/><label>dump</label>
					<input type="radio" name="display" value="table" <?php if(isset($_GET['display']) && $_GET['display']=='table')  echo 'checked="checked"';?>/><label>table</label>
					<input type="radio" name="display" value="nested-list" <?php if(isset($_GET['display']) && $_GET['display']=='nested-list')  echo 'checked="checked"';?>/><label>nested list</label>
					<input type="radio" name="display" value="nested-table" <?php if(isset($_GET['display']) && $_GET['display']=='nested-table')  echo 'checked="checked"';?>/><label>nested table</label>
					<!-- <input type="radio" name="display" value="js-tree" <?php if(isset($_GET['display']) && $_GET['display']=='js-tree')  echo 'checked="checked"';?>/><label>js tree</label> -->
					<input type="radio" name="display" value="mini-nested-table" <?php if(isset($_GET['display']) && $_GET['display']=='mini-nested-table')  echo 'checked="checked"';?>/><label>mini nested table <i>(works for aktualne.sk only)</i></label>

				</td>
			</tr>
			<tr>
				<td class="leftn"></td><td class="rightn"><input class="button" type="submit" value="Submit" onmouseover="color(this)" onmouseout="uncolor(this)"/> <input class="button" type="reset" value="Reset" onmouseover="color(this)" onmouseout="uncolor(this)"/></td>
			</tr>
		</table>
	</form><br/><hr><br/>
	<code>
	<?php
		ini_set("display_errors", 0);
		require "../Helpers/GetNodeHelperReferaty.php";
		require "../Helpers/GetNodeHelperAktualne.php";
		require "../Helpers/CurlHelper.php";
		require "../Helpers/DisplayHelperReferaty.php";
		require "../Helpers/DisplayHelperAktualne.php";
		require "../Helpers/DocumentUrlHelper.php";
		require "../Constants/NodeViewerConstants.php";
		//require "../Helpers/IDisplayHelper.php";
		
		if (isset($_GET['hessian'])) {$hessianUrl=$_GET['hessian'];} else {$hessianUrl="";};
		if (isset($_GET['doc_url'])) {$documentUrl=$_GET['doc_url'];} else {$documentUrl="";};
		if (isset($_GET['article_id'])) {$articleId=$_GET['article_id'];} else {$articleId="";};
		if (isset($_GET['public_uri'])) {$publicUri=$_GET['public_uri'];} else {$publicUri="";};
		if (isset($_GET['revision'])) {$revision=$_GET['revision'];} else {$revision="";};
		if (isset($_GET['display'])) {$display=$_GET['display'];} else {$display="";};
		
		switch ($hessianUrl) {
			case "http://10.10.20.53:14090/ZikkuratCoreReferatyApp/public/NodeManagerService":
				$getNode=GetNodeHelperReferaty::getNodeCurl($hessianUrl,$documentUrl,$articleId,$publicUri,$revision,$display);
				break;
			case "http://fs.atlas-as.sk:8000/ZikkuratAktualneApp/NodeManagerService":
				$getNode=GetNodeHelperAktualne::getNodeCurl($hessianUrl,$documentUrl,$articleId,$publicUri,$revision,$display);
				break;
			case "http://10.10.20.51:14080/ZikkuratCore/NodeManagerService":
				$getNode=GetNodeHelperAktualne::getNodeCurl($hessianUrl,$documentUrl,$articleId,$publicUri,$revision,$display);
				break;
			case "http://app-java-01.in.new.atlas-as.sk:14080/ZikkuratCore/NodeManagerService":
				$getNode=GetNodeHelperAktualne::getNodeCurl($hessianUrl,$documentUrl,$articleId,$publicUri,$revision,$display);
				break;
			default:
				echo "No Hessian URL to be connected to.";
		}
		
	?>
	</code>
</body>
</html>
