<?php

ini_set("max_execution_time", 0);

interface NodeManager {
	public function addNode($url);
	public function getNode($url);
	public function getNodes($urls);
	public function getNodeRevision($url, $revision);
	public function getNodeRevisions($url);
	public function saveNode($node);
	public function refreshIndexes();
}

class Node {
	public $name;
	public $url;
	public $primaryType;
	public $mixinTypes;
	public $properties;
	public $children;
	
	public function Node(){
		$this->mixinTypes = array();
		$this->properties = array();
		$this->children = array();
	}
	
	public function addNode($name){
		$node = new Node();
		$node->setName($name);
		$node->setUrl($this->url . "/" . $name);
		$this->children[$name] = $node;
		return $node;
	}

	public function addProperty($name){
		$property = new Property();
		$property->setName($name);
		$this->properties[$name] = $property;
		return $property;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name){
		$this->name = $name;
	}
	
	public function getUrl() {
		return $this->url;
	}
	
	public function setUrl($url){
		$this->url = $url;
	}
	
	public function getPrimaryType() {
		return $this->primaryType;
	}
	
	public function setPrimaryType(NodeType $primaryType) {
		$this->primaryType = $primaryType;
	}
	
	public function getPrimaryTypeName(){
		if($this->primaryType == null){
			return null;
		}
		return $this->primaryType->getName();
	}
	
	public function getMixinTypes() {
		return $this->mixinTypes;
	}
	
	public function setMixinTypes($mixinTypes) {
		$this->mixinTypes = $mixinTypes;
	}
	
	public function getProperties() {
		return $this->properties;
	}
	
	public function setProperties($properties) {
		$this->properties = $properties;
	}

	public function getProperty($name){
		if($this->properties == null){
			return null;
		}
		

		return $this->properties[$name];
	}

	public function getPropertyValue($name){
		if($this->properties == null){
			return null;
		}
		if(!isset($this->properties[$name]))
			return null;
		if($this->properties[$name] == null){
			return null;
		}
		return $this->properties[$name]->getValue();
	}

	public function getPropertyValueAsString($name){
		if($this->properties == null){
			return null;
		}
		if(!isset($this->properties[$name]))
			return null;
		if($this->properties[$name] == null){
			return null;
		}
		if($this->properties[$name]->getValue() == null){
			return null;
		}
		if($this->properties[$name]->getValue() == ""){
			return ""; 
		}
		return $this->properties[$name]->getValue()->getString();
	}

	public function getPropertyValueAsDecimal(){
		if($this->properties == null){
			return null;
		}
		if(!isset($this->properties[$name]))
			return null;
		if($this->properties[$name] == null){
			return null;
		}
		if($this->properties[$name]->getValue() == null){
			return null;
		}
		return $this->properties[$name]->getValue()->getDecimal();
	}

	public function getPropertyValueAsDouble(){
		if($this->properties == null){
			return null;
		}
		if(!isset($this->properties[$name]))
			return null;
		if($this->properties[$name] == null){
			return null;
		}
		if($this->properties[$name]->getValue() == null){
			return null;
		}
		return $this->properties[$name]->getValue()->getDouble();
	}

	public function getChild($name){
		if(!isset($this->children)) {
			return null;		
		}
		if($this->children == null){
			return null;
		}
		if(!isset($this->children[$name]))
			return null;

		return $this->children[$name];
	}
	
	public function getChildren() {
		return $this->children;
	}
	
	public function setChildren($children) {
		$this->children = $children;
	}
}

class NodeType {

	public $name;
	
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
}

class Property {

	public $name;
	public $value;
	
	public function Property (){
		$this->value = array();
	}

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	
	/*public function addValue(Value $value){
		array_push($this->value,$value);
	}*/

	public function addValue($stringVal){
		$v = new Value();
		$v->setString($stringVal);
		$this->addValue($v);
	}

	/*public function addValue(Long $bigDecimal){
		$v = new Value();
		$v->setDecimal($bigDecimal);
		$this->addValue($v);
	}

	public function addValueAsDouble($doublee){
		$v = new Value();
		$v.setDouble($doublee);
		$this->addValue($v);
	}*/
	
	public function getValues() {
		return $this->value;
	}

	public function getValue(){
		return $this->value[0];
	}
	
	public function setValues($value) {
		$this->value = $value;
	}
	
}

class Value {

	public $type;
	public $binary;
	public $bool;
	public $date;
	public $decimal;
	public $doublee;
	public $longg;
	public $string;
	
	public function getType() {
		return $this->type;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function getBinary() {
		return $this->binary;
	}
	public function setBinary($binary) {
		$this->binary = $binary;
	}
	public function isBoolean() {
		return $this->bool;
	}
	public function setBoolean($bool) {
		$this->bool = $bool;
	}
	public function getBoolean() {
		return $this->bool;
	}
	public function getDate() {
		return $this->date;
	}
	public function setDate($date) {
		$this->date = $date;
	}
	public function getDecimal() {
		return $this->decimal;
	}
	public function setDecimal($decimal) {
		$this->decimal = $decimal;
	}
	public function getDouble() {
		return $this->doublee;
	}
	public function setDouble($doublee) {
		$this->doublee = $doublee;
	}
	public function getLong() {
		return $this->longg;
	}
	public function setLong($longg) {
		$this->longg = $longg;
	}
	public function getString() {
		return $this->string;
	}
	public function setString($string) {
		$this->string = $string;
	}
}

?>
