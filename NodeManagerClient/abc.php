<!DOCTYPE html>
<html>
<head>
<title>Node Viewer</title>
<link rel="stylesheet" type="text/css" href="client.css">
</head>
<body>
	<form method="post">
		Document URL: <input type="text" class="client" name="doc_url"/><br/>
		<input type="submit"/>
	</form><br/><br/>
	<?php	
		ini_set('memory_limit', '-1');
		ini_set('display_errors', 1); 
		error_reporting(E_ALL);
		include_once("../NodeTest/context.php");
ini_set("max_execution_time", 0);
		
		$context = new CreateArticleContext();

		$node = $context->getNode($_POST['doc_url']);
		echo "<pre>";
		var_dump($node);
		echo "</pre>";
	?>
</body>
</html>
