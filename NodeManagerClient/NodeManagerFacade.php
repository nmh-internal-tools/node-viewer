<?php

include "NodeManagerClient.php";
ini_set("max_execution_time", 0);

class NodeManagerFacade {

	private $nodeManagerService;

	public function NodeManagerFacade() {
	   	$this->nodeManagerService =  NodeManagerClient::get();	
	}

	public function getNode($documentUrl) {
		return $this->nodeManagerService->getNode($documentUrl);	
	}




}

?>
