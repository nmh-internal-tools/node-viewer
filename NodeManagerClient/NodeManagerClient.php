<?php

include "../Hessian/HessianClient.php";
include dirname(__FILE__)."/NodeManager.php";
ini_set("max_execution_time", 0);

class NodeManagerClient implements NodeManager {
	
	private static $SERVICE_URL = "http://10.10.20.56:14090/ZikkuratCoreReferatyApp/public/NodeManagerService";
	
	private static $singleton;
	private $proxy;
	
	public function NodeManagerClient(){
		$options = new HessianOptions();
		$options->typeMap['Node'] = 'sk.centrumholdings.zikkurat.api.nodes.Node';
		$options->typeMap['NodeType'] = 'sk.centrumholdings.zikkurat.api.nodes.NodeType';
		$options->typeMap['Property'] = 'sk.centrumholdings.zikkurat.api.nodes.Property';
		$options->typeMap['Value'] = 'sk.centrumholdings.zikkurat.api.nodes.Value';
		$this->proxy = new HessianClient(NodeManagerClient::$SERVICE_URL,$options);
	}
	
	public function addNode($url){
		return $this->proxy->addNode($url);
	}
	
	public function getNode($url){
		return $this->proxy->getNode($url);
	}
	
	public function getNodes($urls){
		return $this->proxy->getNodes($urls);
	}
	
	public function getNodeRevision($url, $revision) {
		return $this->proxy->getNodeRevision($url, $revision);
	}
	
	public function getNodeRevisions($url) {
		return $this->proxy->getNodeRevisions($url);
	}
	
	public function saveNode($node){
		$this->proxy->saveNode($node);
	}
	
	public function refreshIndexes() {
		$this->proxy->refreshIndexes();
	}
	
	public static function get(){
		if(NodeManagerClient::$singleton == null){
			NodeManagerClient::$singleton = new NodeManagerClient();
		}
		return NodeManagerClient::$singleton;
	}
	
}

?>
