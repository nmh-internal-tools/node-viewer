<?php
require "../Helpers/CurlHelper.php";

$url = "http://10.10.20.56:14090/ZikkuratCoreReferatyApp/public/NodeManagerService";
$fields = array();
$method = "post";

$checkHessian = CurlHelper::getCurlResponse($url, $fields, $method);

switch ($checkHessian['http_status']) {
	case 200:
		$status = "OK";
		break;
	case 404:
		$status = "Not Found";
		break;
	case 503:
		$status = "Service unavailable";
		break;
	case 504:
		$status = "Gateway timeout";
		break;
	case 500:
		$status = "Internal Server Error";
		break;
	default:
		$status = "Undefined error";
		break;
}

echo "<strong>hessian.status</strong>: ".$status."<br/>";
echo "<strong>hessian.response.code</strong>: ".$checkHessian['http_status']."<br/>";
echo "<strong>hessian.error.no</strong>: ".$checkHessian['errno']."<br/>";
echo "<strong>hessian.error.message</strong>: ".$checkHessian['error']."<br/>";
?>