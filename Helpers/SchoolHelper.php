<?php

ini_set("max_execution_time", 0);

interface ISchoolHelper{
	function getSchoolData($schoolId);
	function getAllSchoolData();
}

class SchoolHelper implements ISchoolHelper{

	private $schoolUrl = "/export/www/www.centrum.sk/www/SCR/Referaty/Support/School/schools.json";	
	private $_schoolData = null;
	
	public function __construct(){

	}
	
	public function getSchoolData($schoolId){
		$schoolData = $this->getAllSchoolData();
		$result = array();
		if(!isset($schoolData["schools"][$schoolId])){
			throw new Exception("School ID not exists (ID: ".$currentCategoryId.")");
		}
		$result = $schoolData["schools"][$schoolId];
		return $result;
	}

	public function getAllSchoolData(){
		if($this->_schoolData == null){
			$this->_schoolData = json_decode(file_get_contents($this->schoolUrl),true);
			
		}
		return $this->_schoolData;
	}
	
	private function getAllSchoolDataCURL(){
		if($this->_schoolData == null){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $this->schoolUrl); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		 	$data = curl_exec($curl); 
			curl_close($curl); 
			$this->_schoolData = json_decode($data, true);
		}
		return $this->_schoolData;
	}
	
	
	public static function get(){
		return new SchoolHelper();
	}
}

?>
