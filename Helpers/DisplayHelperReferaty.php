<?php
    ini_set("display_errors", 1);
	
	require "IDisplayHelper.php";
	
	class DisplayHelperReferaty implements IDisplayHelper {
		
		public $arrayDepth;
		
		function __construct($node) {
			$this->arrayDepth = $this->arrayDepthCalculator($node);
		}
		
		public function dumpData($node,$articleId) {
			echo "<pre>";
			var_dump($node);
			echo "</pre>";
			echo "<strong>Images to display:</strong><br/>";
			$this->fetchImages($articleId);
		}
		
		public function displayTable($node,$publicUri) {
			$this->fetchTable($node,$publicUri);
		}
		
		public function getNestedList($node) {
			$this->fetchNestedList($node);
		}
		
		public function getNestedTable($node) {
			echo "Node array depth is: <strong>".$this->arrayDepth." levels</strong>.<br/>";
			$this->fetchNestedTable($node);
			echo "<strong>Images to display:</strong><br/>";
			$articleId=$node['properties']['article_id']['value']['0']['string'];
			$this->fetchImages($articleId);
		}
		/*
		public function getJsTree($node) {
			$this->fetchJsTree($node);
		}
		*/
		private function fetchImages($article_id) {
			$imagePath="http://referaty.atlas.sk/images/referaty/images/";
			$images=$this->getImageData($article_id);
			if ($this->checkImages($images)!=false) {
				foreach ($images as $image) {
					foreach ($image as $i) {
						if ($this->checkImageUrl($i['url'])!=false) {
							echo "<img src='".$imagePath.$i['url']."' alt='image_".$i['id']."'/><br/>";
							echo $i['id']."<br/>";
							echo "<br/><br/>";
						}	
					}
				}
			}
		}
		
		private function checkImages($images) {
			try {
				if ($images==null || $images=="") {
					throw new Exception("No images to display.");
				}
			}
			catch (Exception $e) {
				echo $e->getMessage()."\n";
				return false;
			}
		}
		
		private function checkImageUrl($imageUrl) {
			try {
				$a=preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $imageUrl);
				if ($a!==1) {
					echo "<img src='no_image_available.png' alt='no_image_available' width='150' height='150'/><br/>";
					throw new Exception("URL of this image is not valid.");
					echo "<br/><br/>";
				}
			}
			catch (Exception $e) {
				echo $e->getMessage()."\n";
				return false;
			}
		}
		
		private function getImageData($id) {
			$path="http://www.centrum.sk/SCR/Referaty/Support/Zikkurat-1.5/ArticleImage/get-images.php";
			$getCurlResponse=CurlHelper::getCurlResponse($path,array("id"=>urlencode($id)),"get");
			if($getCurlResponse['http_status']==200) {
				return json_decode($getCurlResponse['result'], true);
			}	
			else {
				return false;
			}
		}
		
		private function fetchTable($node,$publicUri) { ?>
			<table>
				<tr>
					<td class="left">node:</td><td> <?php echo $node['url']; ?> </td>
				</tr>
				<tr>
					<td class="left">title:</td><td> <?php echo $node['properties']['title']['value'][0]['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">public URI:</td><td> <?php echo $publicUri; ?> </td>
				</tr>
				<tr>
					<td class="left">category:</td><td> <?php echo $node['properties']['category']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">original_category:</td><td> <?php echo $node['properties']['original_category']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">displaytime:</td><td> <?php $timestamp=$node['properties']['displaytime']['value']['0']['longg']; echo $timestamp." (".gmdate("d-m-Y\ H:i:s", $timestamp/1000).")"; ?> </td>
				</tr>
				<tr>
					<td class="left">keywords:</td><td> <?php echo $node['properties']['keywords']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">state:</td><td> <?php echo $node['properties']['state']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">space:</td><td> <?php echo $node['properties']['space']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">id_article_space:</td><td> <?php echo $node['properties']['id_article_space']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">article_id:</td><td> <?php echo $node['properties']['article_id']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">legacy_id:</td><td> <?php echo $node['properties']['legacy_id']['value']['0']['longg']; ?> </td>
				</tr>
				<tr>
					<td class="left">user_id:</td><td> <?php echo $node['properties']['user_id']['value']['0']['longg']; ?> </td>
				</tr>
				<tr>
					<td class="left">user_id_islegacy:</td><td> <?php echo $node['properties']['user_id_islegacy']['value']['0']['bool']; ?> </td>
				</tr>
				<tr>
					<td class="left">language:</td><td> <?php echo $node['properties']['language']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">note:</td><td> <?php echo $node['properties']['note']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">images:</td><td> <?php $articleId=$node['properties']['article_id']['value']['0']['string']; $this->fetchImages($articleId); ?> </td>
				</tr>
				<tr>
					<td class="left">sources:</td><td><i>--TBD--</i></td>
				</tr>
				<tr>
					<td class="left">content:</td><td> <?php echo $node['properties']['content2']['value']['0']['string']; ?> </td>
				</tr>
			</table>
		<?php
		}

		private function fetchNestedList($node) {
			echo "Node array depth is: <strong>".$this->arrayDepth." levels</strong>.<br/>";
			echo "<ul>";
			$l1 = 0;
			foreach ($node as $key1 => $value1) {
				++$l1;
				echo "<li>";
				if (!is_array($value1)) {					
					echo $l1." <strong>".$key1.":</strong> ".$value1;
					echo "</li>";
				}
				else {
					echo $l1." <strong>".$key1.":</strong><br/>";
					echo "<ul>";
					$l2 = 0;
					foreach ($value1 as $key2 => $value2) {
						++$l2;
						echo "<li>";
						if (!is_array($value2)) {
							echo $l1.".".$l2." <strong>".$key2.":</strong> ".$value2;
							echo "</li>";
						}
						else {
							echo $l1.".".$l2." <strong>".$key2.":</strong><br/>";
							echo "<ul>";
							$l3 = 0;
							foreach ($value2 as $key3 => $value3) {
								++$l3;
								echo "<li>";
								if (!is_array($value3)) {
									echo $l1.".".$l2.".".$l3." <strong>".$key3.":</strong> ".$value3;
									echo "</li>";
								}
								else {
									echo $l1.".".$l2.".".$l3." <strong>".$key3.":</strong><br/>";
									echo "<ul>";
									$l4 = 0;
									foreach ($value3 as $key4 => $value4) {
										++$l4;
										echo "<li>";
										if (!is_array($value4)) {
											echo $l1.".".$l2.".".$l3.".".$l4." <strong>".$key4.":</strong> ".$value4;
											echo "</li>";
										}
										else {
											echo $l1.".".$l2.".".$l3.".".$l4." <strong>".$key4.":</strong><br/>";
											echo "<ul>";
											$l5 = 0;
											foreach ($value4 as $key5 => $value5) {
												++$l5;
												echo "<li>";
												if (!is_array($value5)) {
													echo $l1.".".$l2.".".$l3.".".$l4.".".$l5." <strong>".$key5.":</strong> ".$value5;
													echo "</li>";
												}
												else {
													echo $l1.".".$l2.".".$l3.".".$l4.".".$l5." <strong>".$key5.":</strong><br/>";
													echo "<ul>";
													$l6 = 0;
													foreach ($value5 as $key6 => $value6) {
														++$l6;
														echo "<li>";
														if (!is_array($value6)) {
															echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6." <strong>".$key6.":</strong> ".$value6;
															echo "</li>";
														}
														else {
															echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6." <strong>".$key6.":</strong><br/>";
															echo "<ul>";
															$l7 = 0;
															foreach ($value6 as $key7 => $value7) {
																++$l7;
																echo "<li>";
																if (!is_array($value7)) {
																	echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7." <strong>".$key7.":</strong> ".$value7;
																	echo "</li>";
																}
																else {
																	echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7." <strong>".$key7.":</strong><br/>";
																	echo "<ul>";
																	$l8 = 0;
																	foreach ($value7 as $key8 => $value8) {
																		++$l8;
																		echo "<li>";
																		if (!is_array($value8)) {
																			echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8." <strong>".$key8.":</strong> ".$value8;
																			echo "</li>";
																		}
																		else {
																			echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8." <strong>".$key8.":</strong><br/>";
																			echo "<ul>";
																			$l9 = 0;
																			foreach ($value8 as $key9 => $value9) {
																				++$l9;
																				echo "<li>";
																				if (!is_array($value9)) {
																					echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9." <strong>".$key9.":</strong> ".$value9;
																					echo "</li>";
																				}
																				else {
																					echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9." <strong>".$key9.":</strong><br/>";
																					echo "<ul>";
																					$l10 = 0;
																					foreach ($value9 as $key10 => $value10) {
																						++$l10;
																						echo "<li>";
																						if (!is_array($value10)) {
																							echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9.".".$l10." <strong>".$key10.":</strong> ".$value10;
																							echo "</li>";
																						}
																						else {
																							
																						}
																						echo "</li>";
																					}
																					echo "</ul>";
																				}
																				echo "</li>";
																			}
																			echo "</ul>";
																		}
																		echo "</li>";
																	}
																	echo "</ul>";
																}
																echo "</li>";
															}
															echo "</ul>";
														}
														echo "</li>";
													}
													echo "</ul>";
												}
												echo "</li>";
											}
											echo "</ul>";
										}
										echo "</li>";
									}
									echo "</ul>";
								}
								echo "</li>";
							}
							echo "</ul>";
						}
						echo "</li>";
					}
					echo "</ul>";
				}
				echo "</li>";
			}
			echo "</ul>";
			echo "<strong>Images to display:</strong><br/>";
			$articleId=$node['properties']['article_id']['value']['0']['string']; $this->fetchImages($articleId);
		}

		private function fetchNestedTable($node) {
			echo "<table>";
			foreach ($node as $key => $value) {
				echo "<tr>";
				echo "<td>";
				if (!is_array($value)) {
					echo "<tr>";					
					echo "<td class='left'>".$key.":</td><td> ".$value."</td>";
					echo "</tr>";
				}
				else {
					echo "<tr>";
					echo "<td class='left'>".$key.":</td>";
					echo "<td>";
					$this->fetchNestedTable($value);
					echo "</td>";
					echo "</tr>";
				}
				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
/*
		private function fetchJsTree($node) {
			echo "<ul>";
			foreach ($node as $key => $value) {
				echo "<li>";
				if (!is_array($value)) {		
					echo "<span class='left'>".$key.": </span><span>".$value."</span>";
				}
				else {	
					echo "<span class='left'><a href='javascript:toggleList()'>".$key.":</a></span><span class='hiddenspan'>";
					echo "<ul>";
					foreach ($value as $key1 => $value1) {
						echo "<li>";
						if (!is_array($value1)) {		
							echo "<span class='left'>".$key1.": </span><span>".$value1."</span>";
						}
						else {	
							echo "<span class='left'><a href='javascript:toggleList()'>".$key1.":</a></span><span class='hiddenspan'>".$this->fetchJsTree($value1)."</span>";
						}
						echo "</li>";
					}
					echo "</ul>";
					echo "</span>";
				}
				echo "</li>";
			}
			echo "</ul>";
		}
*/
		private function arrayDepthCalculator(array $array) {
		    $maxDepth = 1;
		    foreach ($array as $value) {
		        if (is_array($value)) {
		            $depth = $this->arrayDepthCalculator($value) + 1;
		            if ($depth > $maxDepth) {
		                $maxDepth = $depth;
		            }
		        }
		    }
		    return $maxDepth;
		}
	}
?>