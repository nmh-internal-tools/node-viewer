<?php
ini_set("display_errors",1);
interface ICategoryHelper{
	function getCategoryTree($category);
	function getCategoryData();
}

class CategoryHelper implements ICategoryHelper{

	private $categoryUrl = "/export/www/www.centrum.sk/www/SCR/Referaty/Support/Category/kategorie.json";	
	private $_categoryData = null;
	
	public function __construct(){

	}
	
	public function getCategoryTree($categoryId){
		$categoryData = $this->getCategoryData();

		$result = array();
		$currentCategoryId = $categoryId;
		while(true){

			if(!isset($categoryData["categories"][$currentCategoryId])){
				throw new Exception("Category ID not exists (ID: ".$currentCategoryId.")");
			}
			$currentCategory = $categoryData["categories"][$currentCategoryId];
			
			array_unshift($result, array(
				"id" => $currentCategoryId,
				"name" => $currentCategory["name"],
				"uri-fragment" => $currentCategory["uri-fragment"]
			));
			if($currentCategory["parent"] == null){
				break;
			}
			$currentCategoryId = $currentCategory["parent"];
		}
		
		return $result;
	}
	
	public function getCategoryData(){
		if($this->_categoryData == null){
			$this->_categoryData = json_decode(file_get_contents($this->categoryUrl),true);
			
			
		}
		return $this->_categoryData;
	}

	private function getCategoryDataCURL(){
		if($this->_categoryData == null){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $this->categoryUrl); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		 	$data = curl_exec($curl); 
			curl_close($curl); 
			$this->_categoryData = json_decode($data, true);
		}
		return $this->_categoryData;
	}
	
	
	public static function get(){
		return new CategoryHelper();
	}
}

?>
