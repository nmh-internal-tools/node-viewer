<?php
ini_set("display_errors", 0);

class CurlHelper {

	public static function getCurlResponse($url,$fields,$method) {
		/*$fields = array(
		'json' => urlencode(json_encode($arrToIndex))
	       );*/

		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init($url);

		$method=strtoupper($method);
		
		if ($method=="POST") {			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute post
			$result = curl_exec($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$redirect_count = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
			//$primary_ip = curl_getinfo($ch, CURLINFO_PRIMARY_IP);
			//$primary_port = curl_getinfo($ch, CURLINFO_PRIMARY_PORT);
			//$local_ip = curl_getinfo($ch, CURLINFO_LOCAL_IP);
			//$local_port = curl_getinfo($ch, CURLINFO_LOCAL_PORT);
			$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
			$errno = curl_errno($ch);
			$error = curl_error($ch);

			curl_close($ch);

			return array(
				'result' => $result,
				'http_status' => $http_status,
				'redirect_count' => $redirect_count,
				//'primary_ip' => $primary_ip,
				//'primary_port' => $primary_port,
				//'local_ip' => $local_ip,
				//'local_port' => $local_port,
				'content_type' => $content_type,
				'errno' => $errno,
				'error' => $error
				);
		}
		elseif ($method=="GET") {
			//set the url
			curl_setopt($ch,CURLOPT_URL, $url."?".$fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute post
			$result = curl_exec($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$redirect_count = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
			//$primary_ip = curl_getinfo($ch, CURLINFO_PRIMARY_IP);
			//$primary_port = curl_getinfo($ch, CURLINFO_PRIMARY_PORT);
			//$local_ip = curl_getinfo($ch, CURLINFO_LOCAL_IP);
			//$local_port = curl_getinfo($ch, CURLINFO_LOCAL_PORT);
			$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
			$errno = curl_errno($ch);
			$error = curl_error($ch);

			curl_close($ch);

			return array(
				'result' => $result,
				'http_status' => $http_status,
				'redirect_count' => $redirect_count,
				//'primary_ip' => $primary_ip,
				//'primary_port' => $primary_port,
				//'local_ip' => $local_ip,
				//'local_port' => $local_port,
				'content_type' => $content_type,
				'errno' => $errno,
				'error' => $error
				);
		}
		else {
			//get HTTP error response code
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			//execute post
			$result = curl_exec($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$redirect_count = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
			//$primary_ip = curl_getinfo($ch, CURLINFO_PRIMARY_IP);
			//$primary_port = curl_getinfo($ch, CURLINFO_PRIMARY_PORT);
			//$local_ip = curl_getinfo($ch, CURLINFO_LOCAL_IP);
			//$local_port = curl_getinfo($ch, CURLINFO_LOCAL_PORT);
			$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
			$errno = curl_errno($ch);
			$error = curl_error($ch);

			curl_close($ch);

			return array(
				'result' => $result,
				'http_status' => $http_status,
				'redirect_count' => $redirect_count,
				//'primary_ip' => $primary_ip,
				//'primary_port' => $primary_port,
				//'local_ip' => $local_ip,
				//'local_port' => $local_port,
				'content_type' => $content_type,
				'errno' => $errno,
				'error' => $error
				);
		}
	}
}

?>
