<?php
    ini_set("display_errors", 1);
	
	interface IDisplayHelper {
		
		function getNestedList($node);
		function getNestedTable($node);
		//function getMiniNestedTable($node);
	}
?>