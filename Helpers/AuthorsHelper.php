<?php
ini_set('display_errors', 0); 
include "../Helpers/CurlHelper.php";
//include "../Constants/Constants.php";
ini_set("max_execution_time", 0);

class AuthorHelper {
	
	/*
	public static function getAuthorData($id) {
		$path="http://10.10.20.56:16001/Referaty/Support/GetAuthor?id=".$id;
		 $content = file_get_contents($path);
		if($content == false) {
			return array();		
		}	
		else {
			return json_decode($content, true);
		}
		
	}
	

	public static function getAuthorData($id) {
		$path="http://10.10.20.56:16001/Referaty/Support/GetAuthor";
		//$content = file_get_contents($path);
		$getCurlResponse=CurlHelper::getCurlResponse($path,array("id"=>urlencode($id)),"POST");
		//echo "<pre>";		
		//var_dump($getCurlResponse);
		//echo "</pre>";
		if($getCurlResponse==false) {
			header("HTTP/1.0 404 Not Found");	
		}	
		else {
			return json_decode($getCurlResponse, true);
		}
	}
	*/

	public static function getAuthorData($id) {
		$path="http://10.10.20.56:16001/Referaty/Support/GetAuthor";
		//$content = file_get_contents($path);
		$getCurlResponse=CurlHelper::getCurlResponse($path,array("id"=>urlencode($id)),"get");
		//echo "<pre>";		
		//var_dump($getCurlResponse);
		//echo "</pre>";
		if($getCurlResponse['http_status']==200) {
			return json_decode($getCurlResponse['result'], true);
		}	
		else {
			return array("isOk"=>false,"reason"=>$getCurlResponse['http_status']);
		}
	}
}

?>
