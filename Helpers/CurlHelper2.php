<?

ini_set("max_execution_time", 0);

class CurlHelper {

	public static function getCurlResponse($url,$fields,$method) {
		
		/*$fields = array(
		'json' => urlencode(json_encode($arrToIndex))
	       );*/

		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init($url);

		$method=strtoupper($method);
		if ($method=="POST") {			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute post
			$result = curl_exec($ch);

			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);

			return array('result'=>$result, 'http_status'=>$http_status);
		}
		elseif ($method=="GET") {
			//set the url
			curl_setopt($ch,CURLOPT_URL, $url."?".$fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute post
			$result = curl_exec($ch);

			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);
			return array('result'=>$result, 'http_status'=>$http_status);
		}
		else {
			//get HTTP error response code
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			return array('result'=>"", 'http_status'=>$http_status);
		}
	}
}

?>
