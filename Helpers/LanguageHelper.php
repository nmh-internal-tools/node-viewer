<?php

ini_set("max_execution_time", 0);

interface ILanguageHelper {
	public function getDetail($id);
}

//For current use: filesystem implementation
class LanguageHelper implements ILanguageHelper {

	private $dataDir = "/export/www/www.centrum.sk/www/SCR/Referaty/Support/Language/";
	private $validIds = array("anglictina", "cestina", "francuzstina", "iny", "madarcina",
						"nemcina", "rustina", "slovencina", "spanielcina", "taliancina");
						
	private function isFileExists($fullPath) {		
		return file_exists($fullPath);
	}

	public function getDetail($id) {

		if(!$this->isValidIdInput($id)){
			throw new Exception("Not valid ID");
		}
		$fullPath = $this->dataDir.$id.".json";
		if($this->isFileExists($fullPath)){
			return json_decode(file_get_contents($fullPath), true);	
		}else{
			throw new Exception("File not found");
		}
	} 
	
	private function isValidIdInput($id){
		if(in_array($id, $this->validIds)){
			return true;
		}else{
			return false;
		}
	}
	
	public static function get(){
		return new LanguageHelper();
	}
}

?>
