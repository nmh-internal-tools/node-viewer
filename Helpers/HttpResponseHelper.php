<?php
namespace Http;
class HttpResponseHelper {
	
	public static function noCache() {
			
	}

	public static function clientResponse($responseType,$content="") {
		header("Cache-Control: no-cache, must-revalidate");

		switch ($responseType) {
		case "404":
			header("HTTP/1.0 404 Not Found");
			break;
		case "json":
			header('Content-Type: application/json;charset=utf-8');
			echo $content;
		}

			
	}
}