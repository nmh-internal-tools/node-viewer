<?php
    ini_set("display_errors", 1);
	
	//require "IDisplayHelper.php";
	
	class DisplayHelperAktualne implements IDisplayHelper {
		
		public $arrayDepth;
		
		function __construct($node) {
			$this->arrayDepth = $this->arrayDepthCalculator($node);
		}
		
		public function dumpData($node) {
			echo "<pre>";
			var_dump($node);
			echo "</pre>";
			$panoramas=$node['children']['panoramas']['children'];
			echo "<strong>Images to display:</strong><br/>";
			$this->fetchImages($panoramas);
		}
		
		public function displayTable($node) {
			$this->fetchTable($node);
		}
		
		public function getNestedList($node) {
			$this->fetchNestedList($node);
		}
		
		public function getNestedTable($node) {
			echo "Node array depth is: <strong>".$this->arrayDepth." levels</strong>.<br/>";
			$this->fetchNestedTable($node);
			echo "<strong>Images to display:</strong><br/>";
			$panoramas=$node['children']['panoramas']['children']; $this->fetchImages($panoramas);
		}
		
		public function getMiniNestedTable($node) {
			echo "Node array depth is: <strong>".$this->arrayDepth." levels</strong>.<br/>";
			echo "Only main levels are displayed.<br/><br/>";
			$this->fetchMiniNestedTable($node);
			echo "<strong>Images to display:</strong><br/>";
			$panoramas=$node['children']['panoramas']['children']; $this->fetchImages($panoramas);
		}
		/*
		public function getJsTree($node) {
			$this->fetchJsTree($node);
		}
		*/
		private function fetchImages($panoramas) {
			foreach ($panoramas as $panorama) {
				$imagePath[$panorama['properties']['url']['value'][0]['string']]=$panorama['properties']['description']['value'][0]['string'];
			}
			foreach ($imagePath as $image=>$description) {
				echo "<img src='".$image."' alt='image'/><br/>".$description."<br/><br/>";
			}
		}
		
		private function checkImages($images) {
			try {
				if ($images==null || $images=="") {
					throw new Exception("No images to display.");
				}
			}
			catch (Exception $e) {
				echo $e->getMessage()."\n";
				return false;
			}
		}
		
		private function checkImageUrl($imageUrl) {
			try {
				$a=preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $imageUrl);
				if ($a!==1) {
					echo "<img src='no_image_available.png' alt='no_image_available' width='150' height='150'/><br/>";
					throw new Exception("URL of this image is not valid.");
					echo "<br/><br/>";
				}
			}
			catch (Exception $e) {
				echo $e->getMessage()."\n";
				return false;
			}
		}
		
		private function getImageData($id) {
			$path="http://www.centrum.sk/SCR/Referaty/Support/Zikkurat-1.5/ArticleImage/get-images.php";
			$getCurlResponse=CurlHelper::getCurlResponse($path,array("id"=>urlencode($id)),"get");
			if($getCurlResponse['http_status']==200) {
				return json_decode($getCurlResponse['result'], true);
			}	
			else {
				return false;
			}
		}
		
		private function fetchTable($node) { ?>
			<table>
				<tr>
					<td class="left">node:</td><td> <?php echo $node['url']; ?> </td>
				</tr>
				<tr>
					<td class="left">title:</td><td> <?php echo $node['properties']['title']['value'][0]['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">public URI (article_url):</td><td> <?php echo $node['properties']['article_url']['value'][0]['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">category:</td><td> <?php echo $node['properties']['category']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">original_category:</td><td> <?php echo $node['properties']['original_category']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">original_subcategory:</td><td> <?php echo $node['properties']['original_subcategory']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">displaytime:</td><td> <?php $timestamp=$node['properties']['displaytime']['value']['0']['longg']; echo $timestamp." (".gmdate("d-m-Y\ H:i:s", $timestamp/1000).")"; ?> </td>
				</tr>
				<tr>
					<td class="left">starttime:</td><td> <?php $timestamp=$node['properties']['starttime']['value']['0']['longg']; echo $timestamp." (".gmdate("d-m-Y\ H:i:s", $timestamp/1000).")"; ?> </td>
				</tr>
				<tr>
					<td class="left">created_time:</td><td> <?php $timestamp=$node['properties']['created_time']['value']['0']['longg']; echo $timestamp." (".gmdate("d-m-Y\ H:i:s", $timestamp/1000).")"; ?> </td>
				</tr>
				<tr>
					<td class="left">state:</td><td> <?php echo $node['properties']['state']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">space:</td><td> <?php echo $node['properties']['space']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">id_article_space:</td><td> <?php echo $node['properties']['id_article_space']['value']['0']['longg']; ?> </td>
				</tr>
				<tr>
					<td class="left">article_id:</td><td> <?php echo $node['properties']['article_id']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">dicussion_question:</td><td> <?php echo $node['properties']['discussion_question']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">is_discussion_blocked:</td><td> <?php if($node['properties']['is_discussion_blocked']['value']['0']['bool']==true) {$isDiscussionBlocked='yes';} else {$isDiscussionBlocked='no';} echo $isDiscussionBlocked; ?> </td>
				</tr>
				<tr>
					<td class="left">is_updated:</td><td> <?php if($node['properties']['is_updated']['value']['0']['bool']==true) {$isUpdated='yes';} else {$isUpdated='no';} echo $isUpdated; ?> </td>
				</tr>
				<tr>
					<td class="left">is_exclusive:</td><td> <?php if($node['properties']['is_exclusive']['value']['0']['bool']==true) {$isExclusive='yes';} else {$isExclusive='no';} echo $isExclusive; ?> </td>
				</tr>
				<tr>
					<td class="left">is_exclusive_interview:</td><td> <?php if($node['properties']['is_exclusive_interview']['value']['0']['bool']==true) {$isExclusiveInterview='yes';} else {$isExclusiveInterview='no';} echo $isExclusiveInterview; ?> </td>
				</tr>
				<tr>
					<td class="left">interview_id:</td><td> <?php echo $node['properties']['interview_id']['value']['0']['longg']; ?> </td>
				</tr>
				<tr>
					<td class="left">interview_closed:</td><td> <?php if($node['properties']['interview_closed']['value']['0']['bool']==true) {$interviewClosed='yes';} else {$interviewClosed='no';} echo $interviewClosed; ?> </td>
				</tr>
				<tr>
					<td class="left">annotation:</td><td> <?php echo $node['properties']['annotation']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">offset:</td><td> <?php echo $node['properties']['offset']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">priority_aktualne:</td><td> <?php echo $node['properties']['priority_aktualne']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">priority_sub_brand:</td><td> <?php echo $node['properties']['priority_sub_brand']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">tags:</td><td> <?php echo $node['properties']['tags']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">intern_ks:</td><td> <?php echo $node['properties']['intern_ks']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">inquiry_id:</td><td> <?php echo $node['properties']['inquiry_id']['value']['0']['longg']; ?> </td>
				</tr>
				<tr>
					<td class="left">images:</td><td> <?php $panoramas=$node['children']['panoramas']['children']; $this->fetchImages($panoramas); ?> </td>
				</tr>
				<tr>
					<td class="left">content1:</td><td> <?php echo $node['properties']['content1']['value']['0']['string']; ?> </td>
				</tr>
				<tr>
					<td class="left">content2:</td><td> <?php echo $node['properties']['content2']['value']['0']['string']; ?> </td>
				</tr>
			</table>
		<?php
		}

		private function fetchNestedList($node) {
			echo "Node array depth is: <strong>".$this->arrayDepth." levels</strong>.<br/>";
			echo "<ul>";
			$l1 = 0;
			foreach ($node as $key1 => $value1) {
				++$l1;
				echo "<li>";
				if (!is_array($value1)) {					
					echo $l1." <strong>".$key1.":</strong> ".$value1;
					echo "</li>";
				}
				else {
					echo $l1." <strong>".$key1.":</strong><br/>";
					echo "<ul>";
					$l2 = 0;
					foreach ($value1 as $key2 => $value2) {
						++$l2;
						echo "<li>";
						if (!is_array($value2)) {
							echo $l1.".".$l2." <strong>".$key2.":</strong> ".$value2;
							echo "</li>";
						}
						else {
							echo $l1.".".$l2." <strong>".$key2.":</strong><br/>";
							echo "<ul>";
							$l3 = 0;
							foreach ($value2 as $key3 => $value3) {
								++$l3;
								echo "<li>";
								if (!is_array($value3)) {
									echo $l1.".".$l2.".".$l3." <strong>".$key3.":</strong> ".$value3;
									echo "</li>";
								}
								else {
									echo $l1.".".$l2.".".$l3." <strong>".$key3.":</strong><br/>";
									echo "<ul>";
									$l4 = 0;
									foreach ($value3 as $key4 => $value4) {
										++$l4;
										echo "<li>";
										if (!is_array($value4)) {
											echo $l1.".".$l2.".".$l3.".".$l4." <strong>".$key4.":</strong> ".$value4;
											echo "</li>";
										}
										else {
											echo $l1.".".$l2.".".$l3.".".$l4." <strong>".$key4.":</strong><br/>";
											echo "<ul>";
											$l5 = 0;
											foreach ($value4 as $key5 => $value5) {
												++$l5;
												echo "<li>";
												if (!is_array($value5)) {
													echo $l1.".".$l2.".".$l3.".".$l4.".".$l5." <strong>".$key5.":</strong> ".$value5;
													echo "</li>";
												}
												else {
													echo $l1.".".$l2.".".$l3.".".$l4.".".$l5." <strong>".$key5.":</strong><br/>";
													echo "<ul>";
													$l6 = 0;
													foreach ($value5 as $key6 => $value6) {
														++$l6;
														echo "<li>";
														if (!is_array($value6)) {
															echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6." <strong>".$key6.":</strong> ".$value6;
															echo "</li>";
														}
														else {
															echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6." <strong>".$key6.":</strong><br/>";
															echo "<ul>";
															$l7 = 0;
															foreach ($value6 as $key7 => $value7) {
																++$l7;
																echo "<li>";
																if (!is_array($value7)) {
																	echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7." <strong>".$key7.":</strong> ".$value7;
																	echo "</li>";
																}
																else {
																	echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7." <strong>".$key7.":</strong><br/>";
																	echo "<ul>";
																	$l8 = 0;
																	foreach ($value7 as $key8 => $value8) {
																		++$l8;
																		echo "<li>";
																		if (!is_array($value8)) {
																			echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8." <strong>".$key8.":</strong> ".$value8;
																			echo "</li>";
																		}
																		else {
																			echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8." <strong>".$key8.":</strong><br/>";
																			echo "<ul>";
																			$l9 = 0;
																			foreach ($value8 as $key9 => $value9) {
																				++$l9;
																				echo "<li>";
																				if (!is_array($value9)) {
																					echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9." <strong>".$key9.":</strong> ".$value9;
																					echo "</li>";
																				}
																				else {
																					echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9." <strong>".$key9.":</strong><br/>";
																					echo "<ul>";
																					$l10 = 0;
																					foreach ($value9 as $key10 => $value10) {
																						++$l10;
																						echo "<li>";
																						if (!is_array($value10)) {
																							echo $l1.".".$l2.".".$l3.".".$l4.".".$l5.".".$l6.".".$l7.".".$l8.".".$l9.".".$l10." <strong>".$key10.":</strong> ".$value10;
																							echo "</li>";
																						}
																						else {
																							
																						}
																						echo "</li>";
																					}
																					echo "</ul>";
																				}
																				echo "</li>";
																			}
																			echo "</ul>";
																		}
																		echo "</li>";
																	}
																	echo "</ul>";
																}
																echo "</li>";
															}
															echo "</ul>";
														}
														echo "</li>";
													}
													echo "</ul>";
												}
												echo "</li>";
											}
											echo "</ul>";
										}
										echo "</li>";
									}
									echo "</ul>";
								}
								echo "</li>";
							}
							echo "</ul>";
						}
						echo "</li>";
					}
					echo "</ul>";
				}
				echo "</li>";
			}
			echo "</ul>";
			echo "<strong>Images to display:</strong><br/>";
			$panoramas=$node['children']['panoramas']['children']; $this->fetchImages($panoramas);
		}

		private function fetchNestedTable($node) {
			echo "<table>";
			foreach ($node as $key => $value) {
				echo "<tr>";
				echo "<td>";
				if (!is_array($value)) {
					echo "<tr>";					
					echo "<td class='left'>".$key.":</td><td> ".$value."</td>";
					echo "</tr>";
				}
				else {
					echo "<tr>";
					echo "<td class='left'>".$key.":</td>";
					echo "<td>";
					$this->fetchNestedTable($value);
					echo "</td>";
					echo "</tr>";
				}
				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";	
		}
		
		private function fetchMiniNestedTable($node) {
			echo "<table>";
			foreach ($node as $key => $value) {
				if ($key == 'name') {
					echo "<tr><td class='left'>".$key.":</td><td>".$value."</td></tr>";
				}
			}
			foreach ($node as $key => $value) {
				if ($key == 'url') {
					echo "<tr><td class='left'>".$key.":</td><td>".$value."</td></tr>";
				}
			}
			foreach ($node as $key => $value) {
				if ($key == 'properties') {
					echo "<tr>";
					echo "<td class='left'>";
					echo $key.":";
					echo "</td>";
					echo "<td>";
					echo "<table>";
					foreach ($value as $key1 => $value1) {
						echo "<tr>";
						echo "<td class='left'>";
						echo $key1.":";
						echo "</td>";
						echo "<td>";
						if ($value1['value'][0]['string'] != null) {
							echo $value1['value'][0]['string'];
						}
						elseif ($value1['value'][0]['longg'] != null) {
							echo $value1['value'][0]['longg'];
						}
						elseif ($value1['value'][0]['bool'] != 0) {
							echo true;
						}
						else {
							echo "<i class='red'>property missing</i>";
						}
						echo "</td>";
						echo "</tr>";
					}
					echo "</table>";
					echo "</td>";
					echo "</tr>";
				}
			}
			foreach ($node as $key => $value) {
				if ($key == 'children') {
					echo "<tr>";
					echo "<td class='left'>";
					echo "children:";
					echo "</td>";
					echo "<td>";
					$this->fetchNestedTable($value);
					//$this->getChildren($value);
					echo "</td>";
					echo "</tr>";
				}
			}
			echo "</table>";
		}
		
		// not used due to different structure of node['children'] array in each article
		private function getChildren($children) {
			echo "<table>";
			echo "<tr>";
			echo "<td class='left'>";
			echo "children:";
			echo "</td>";
			echo "<td>";
			echo "<table>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "authors:";
			echo "</td>";
			echo "<td>";
			foreach ($children['authors'] as $keyauth => $valueauth) {
				if ($keyauth == 'properties') {
					foreach ($valueauth as $keyauth1 => $valueauth1) {
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo $keyauth1.":";
						echo "</td>";
						echo "<td>";
						echo $valueauth1['value'][0]['string'];
						echo "</td>";
						echo "</tr>";
						echo "</table>";
					}
				}
			}
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "panoramas:";
			echo "</td>";
			echo "<td>";
			foreach ($children['panoramas'] as $keypan => $valuepan) {
				if ($keypan == 'children') {
					foreach ($valuepan as $keypan1 => $valuepan1) {
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo $keypan1.":";
						echo "</td>";
						echo "<td>";
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "id:";
						echo "</td>";
						echo "<td>";
						echo $valuepan1['properties']['id']['value'][0]['longg'];
						echo "</td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "description:";
						echo "</td>";
						echo "<td>";
						echo $valuepan1['properties']['description']['value'][0]['string'];
						echo "</td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "url:";
						echo "</td>";
						echo "<td>";
						echo $valuepan1['properties']['url']['value'][0]['string'];
						echo "</td>";
						echo "</tr>";
						echo "</table>";
						echo "</td>";
						echo "</tr>";
						echo "</table>";
					}
				}
			}
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "related_articles:";
			echo "</td>";
			echo "<td>";
			// related_articles
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "videos:";
			echo "</td>";
			echo "<td>";
			foreach ($children['videos'] as $keyvid => $valuevid) {
				if ($keyvid == 'children') {
					foreach ($valuevid as $keyvid1 => $valuevid1) {
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo $keyvid1.":";
						echo "</td>";
						echo "<td>";
						echo $valuevid1['properties']['embed']['value'][0]['longg'];
						echo "</td>";
						echo "</tr>";
						echo "</table>";
					}
				}
			}
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "related_gallery:";
			echo "</td>";
			echo "<td>";
			echo "<table>";
			echo "<tr>";
			echo "<td class='left'>";
			echo "id:";
			echo "</td>";
			echo "<td>";
			echo $children['related_gallery']['properties']['id']['value'][0]['longg'];
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td class='left'>";
			echo "space:";
			echo "</td>";
			echo "<td>";
			echo $children['related_gallery']['properties']['space']['value'][0]['longg'];
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "thumbnails:";
			echo "</td>";
			echo "<td>";
			foreach ($children['thumbnails'] as $keythumb => $valuethumb) {
				if ($keythumb == 'children') {
					foreach ($valuethumb as $keythumb1 => $valuethumb1) {
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo $keythumb1.":";
						echo "</td>";
						echo "<td>";
						echo "<table>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "id:";
						echo "</td>";
						echo "<td>";
						echo $valuethumb1['properties']['id']['value'][0]['longg'];
						echo "</td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "description:";
						echo "</td>";
						echo "<td>";
						if ($valuethumb1['properties']['is_set']['value'][0]['bool'] == 1) {
							echo 'true';
						}
						else {
							echo 'false';
						}
						echo "</td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td class='left'>";
						echo "url:";
						echo "</td>";
						echo "<td>";
						echo $valuethumb1['properties']['url']['value'][0]['string'];
						echo "</td>";
						echo "</tr>";
						echo "</table>";
						echo "</td>";
						echo "</tr>";
						echo "</table>";
					}
				}
			}
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "specials:";
			echo "</td>";
			echo "<td>";
			echo $children['specials']['properties']['export-pluska']['value'][0]['string'];
			echo "</td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td class='left'>";
			echo "super-tags";
			echo "</td>";
			echo "<td>";
			// super-tags
			echo "</td>";
			echo "</tr>";
			
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		}
		
		private function getProperties($properties) {
			echo "<table>";
			echo "<tr>";
			echo "<td class='left'>";
			echo "properties:";
			echo "</td>";
			foreach ($properties as $key => $value) {
				if (!is_array($key)) {
					echo "<td>";
					echo $key;
					echo "</td>";
				}
				else {
					echo "<td>";
					$this->getProperties($key);
					echo "</td>";
				}
			}
			echo "</tr>";
			echo "</table>";
		}

/*
		private function fetchJsTree($node) {
			echo "<ul>";
			foreach ($node as $key => $value) {
				echo "<li>";
				if (!is_array($value)) {		
					echo "<span class='left'>".$key.": </span><span>".$value."</span>";
				}
				else {	
					echo "<span class='left'><a href='javascript:showList()'>".$key.":</a></span><span id='hiddenspan'>";
					$this->fetchJsTree($value);
					echo "</span>";
				}
				echo "</li>";
			}
			echo "</ul>";
		}
*/
		private function arrayDepthCalculator(array $array) {
		    $maxDepth = 1;
		    foreach ($array as $value) {
		        if (is_array($value)) {
		            $depth = $this->arrayDepthCalculator($value) + 1;
		            if ($depth > $maxDepth) {
		                $maxDepth = $depth;
		            }
		        }
		    }
		    return $maxDepth;
		}
	}
?>