<?php
ini_set('display_errors', 0); 

class ArticleIdHelper {

	public static function isArticleIdValid($articleId) {
		try {
			$checkNew=preg_match('/^([a-z]|[0-9])[a-z|0-9|\-]+/', $articleId);
			var_dump($checkNew);
			$checkOld=preg_match('/^(legacy_)[0-9]+/', $articleId);
			var_dump($checkOld);
			if ($checkNew!=1 && $checkOld!=1) {
				throw new Exception("Article ID is not valid.");
			}
			else {
				return true;
			}
		}
		catch (Exception $e) {
			echo $e->getMessage()."\n";
			return false;
		}
	}
}

?>
