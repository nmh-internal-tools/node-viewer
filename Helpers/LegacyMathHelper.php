<?php

ini_set("max_execution_time", 0);

// Samples of math operations for referaty (15 years old source codes)
class LegacyMathHelper {

	public static $gconf_app_param = array(
		"a4page_word_count" 	=> "1500",
		"a4page_reading" => array(
			"fast" 		=> "100",
			"slow" 		=> "150"
		)
	);

	public static $gutil_wordchars = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789_";

	public static $gutil_utf8_to_ascii = array(
		"ľ" => "l",
		"š" => "s",
		"č" => "c",
		"ť" => "t",
		"ž" => "z",
		"ý" => "y",
		"á" => "a",
		"í" => "i",
		"é" => "e",
		"ú" => "u",
		"ä" => "a",
		"ô" => "o",
		"ň" => "n",
		"ĺ" => "l",
		"ŕ" => "r",
		"ď" => "d",
		"ó" => "o",
		"Ľ" => "L",
		"Š" => "S",
		"Č" => "C",
		"Ť" => "T",
		"Ž" => "Z",
		"Ý" => "Y",
		"Á" => "A",
		"Í" => "I",
		"É" => "E",
		"Ú" => "U",
		"Ä" => "A",
		"Ô" => "O",
		"Ň" => "N",
		"Ĺ" => "L",
		"Ŕ" => "R",
		"Ď" => "D",
		"Ó" => "O",
		"ó" => "o",
		"ö" => "o",
		"Ö" => "O",
		//"&#336;" => "O",
		"ü" => "u",
		"ú" => "u",
		"Ü" => "U",
		"Ú" => "U",
		"´" => "'",
		"ě" => "e",
		"Ě" => "E",
		"ő" => "o",
		"Ő" => "O",
		"ů" => "u",
		"Ů" => "U",
		"ű" => "u",
		"Ű" => "U",
		"ř" => "r",
		"Ř" => "R",
		"’" => "'",
		"à" => "a",
		"À" => "A",
		"è" => "e",
		"È" => "E",
		"ë" => "e",
		"Ë" => "E",
		"ì" => "i",
		"Ì" => "I",
		"ò" => "o",
		"Ò" => "O",
		"ù" => "u",
		"Ù" => "U",
		"“" => "\"",
		"„" => "\"",
		"–" => "-"
	);


	//Function to calculate reading time (slow / fast)
	//@param $a_mode slow / fast
	//@param $a_a4pages count of 4A pages
	public static function _count_reading_time($a_mode, $a_a4pages) {
		$l_fulltime = $a_a4pages * self::$gconf_app_param['a4page_reading'][$a_mode];
		$l_fulltime_min = floor($l_fulltime / 60);
		$l_fulltime_sec = bcmod($l_fulltime, 60);
		return $l_fulltime_min."m ".$l_fulltime_sec."s";
	}


	public static function _count_reading_time_sec($a_mode, $a_a4pages) {
		$l_fulltime = $a_a4pages * self::$gconf_app_param['a4page_reading'][$a_mode];
		return $l_fulltime;
	}

	public static function count_fast_reading_time_sec($a_a4pages) {
		return self::_count_reading_time_sec("fast", $a_a4pages);
	} 

	public static function count_slow_reading_time_sec($a_a4pages) {
		return self::_count_reading_time_sec("slow", $a_a4pages);
	}

	//For fast reading
	public static function count_fast_reading_time($a_a4pages) {
		return self::_count_reading_time("fast", $a_a4pages);
	} 
		

	//For slow reading
	public static function count_slow_reading_time($a_a4pages) {
		return self::_count_reading_time("slow", $a_a4pages);
	}  


	//Calculate how many A4 pages is current html text
	public static function count_pages_in_html($a_html_string) {
		return floor(0.5 + 10 * (self::count_chars_in_html($a_html_string) / self::$gconf_app_param['a4page_word_count'])) / 10;
	}

	//Chars count in html
	public static function count_chars_in_html($a_html_string) {
		$l_string = html_entity_decode(strip_tags(self::convert_to_ascii($a_html_string)));
		$l_count = 0;
		$l_len = strlen($l_string);
		for($i = 0; $i < $l_len; $i++) {
			if(true === self::is_word_char($l_string[$i])) {
				++$l_count;
				continue;
			}
		}
		return $l_count;
	}

	//Ascii helper
	public static function convert_to_ascii($a_string) {
		$l_str = $a_string;
		foreach(self::$gutil_utf8_to_ascii as $l_key => $l_val) {
			$l_str=str_replace($l_key,$l_val,$l_str);
		}
		return $l_str;
	}

	public static function is_word_char($a_char) {
		if(FALSE === strpos(self::$gutil_wordchars, $a_char)) {
			return false;
		} else {
			return true;
		}
	}

	public static function countwords($text){
		$text=strip_tags($text);
		
		return str_word_count($text);
	}
}



?>


