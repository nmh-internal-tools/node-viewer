<?php
    ini_set("display_errors", 0);
	//require "CurlHelper.php";
	//require "../Constants/NodeViewerConstants.php";
	
	class DocumentUrlHelper {
		
		public function getDocumentUrlByArticleId($articleId) {
			$documentPath="http://10.10.20.8/www.centrum.sk/www/SCR/Referaty/Support/ArticleDetail/get-article-detail.php?id=".$articleId;
			$document=json_decode(file_get_contents($documentPath),true);
			$documentUrl=$document['document_url'];
			return $documentUrl;
		}
		
		public function getArticleIdByPublicUri($publicUri) {
			require_once "CurlHelper.php";
			$getCurlResponse=CurlHelper::getCurlResponse("http://admin.atlas.sk/nwo/php53/referaty2014/http-ws-allow/UrlReferatyDB/get-id-by-url.php",array("url"=>$publicUri),"post");
			if ($getCurlResponse['http_status']==200) {
				$result=json_decode($getCurlResponse['result'],true);
				$articleId=$result[0]['article_id'];
				return $articleId;
			}
			else {
				return null;
			}
		}
		
		public static function getPublicUriByArticleId($articleId) {
			$getCurlResponse=CurlHelper::getCurlResponse("http://admin.atlas.sk/nwo/php53/referaty2014/http-ws-allow/UrlReferatyDB/get-url-by-id.php",array("article_id"=>$articleId),"post");
			if ($getCurlResponse['http_status']==200) {
				$result=json_decode($getCurlResponse['result'],true);
				$publicUri=$result[0]['url'];
				return $publicUri;
			}
			else {
				return null;
			}
		}
	}
?>
