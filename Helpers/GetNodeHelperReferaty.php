<?php
    ini_set("display_errors", 1);
	//require "CurlHelper.php";
	//require "DisplayHelper.php";
	//require "DocumentUrlHelper.php";
	//require "../Constants/NodeViewerConstants.php";
	require "IGetNodeHelper.php";
	
	class GetNodeHelperReferaty implements IGetNodeHelper {
		
		public static function getNodeCurl($hessianUrl,$documentUrl,$articleId,$publicUri,$revision,$display) {
			$getCurlResponse=CurlHelper::getCurlResponse(NodeViewerConstants::$getNodePath,array("hessian"=>$hessianUrl,"doc_url"=>$documentUrl,"article_id"=>$articleId,"public_uri"=>$publicUri,"revision"=>$revision,"display"=>$display),"get");
			if ($getCurlResponse['http_status']==200) {
				$node=json_decode($getCurlResponse['result'],true);
				$displayHelper=new DisplayHelperReferaty($node);
				if ($display=="dump") {
					if ($articleId==null || $articleId=="") {
						$articleId=$node['properties']['article_id']['value'][0]['string'];
					}
					$displayHelper->dumpData($node,$articleId);
				}
				elseif ($display=="table") {
					if ($articleId==null || $articleId=="") {
						$articleId=$node['properties']['article_id']['value'][0]['string'];
					}
					if ($publicUri==null || $publicUri=="") {
						$publicUri=DocumentUrlHelper::getPublicUriByArticleId($articleId);
					}
					$displayHelper->displayTable($node,$publicUri);
				}
				elseif ($display=="nested-list") {
					$displayHelper->getNestedList($node);
				}
				elseif ($display=="nested-table") {
					$displayHelper->getNestedTable($node);
				}/*
				elseif ($display=="js-tree") {
					$displayHelper->getJsTree($node);
				}*/
				else {
					echo "Wrong display option.";
				}
			}
			else {
				return null;
			}
		}
	}
?>